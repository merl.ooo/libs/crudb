/* eslint-disable import/no-extraneous-dependencies */
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import clear from 'rollup-plugin-delete';
import dts from 'rollup-plugin-dts';

export default [
  {
    input: 'index.ts',
    plugins: [
      clear({ targets: 'dist' }),
      resolve(),
      commonjs(),
      typescript({ tsconfig: './tsconfig.json' }),
    ],
    // do not bundle lodash + firestore, explicitly bundle nanoid (because its ESM only)
    external: ['@google-cloud/firestore', 'lodash/set', 'lodash/merge'],
    output: [
      {
        file: 'dist/cjs/index.js',
        format: 'cjs',
        exports: 'named',
        sourcemap: true,
      },
      {
        file: 'dist/esm/index.js',
        format: 'esm',
        exports: 'named',
        sourcemap: true,
      },
    ],
    watch: { exclude: ['node_modules/**'] },
  },
  {
    input: 'dist/esm/index.d.ts',
    plugins: [
      dts(),
      !process.env.ROLLUP_WATCH && clear({ targets: 'dist/**/(adapters|*.d.ts)', hook: 'buildEnd' }),
    ],
    output: [{ file: 'dist/index.d.ts', format: 'esm' }],
    external: [/\.css$/],
    watch: { exclude: ['node_modules/**'] },
  },
];

import merge from 'lodash/merge';
import assign from 'lodash/assign';
import set from 'lodash/set';
import { nanoid } from 'nanoid';
import {
  DocumentSnapshot,
  FieldPath,
  Firestore,
  Settings,
  WhereFilterOp,
  CollectionReference,
  Transaction,
  Timestamp,
  Query,
  DocumentData,
} from '@google-cloud/firestore';

import {
  BulkWriteOperation,
  Collection,
  CollectionName,
  Database,
  DatabaseError,
  Document,
  DocumentId,
  DocumentPath,
  FilterQuery,
  FilterQueryOperator,
  StoredDocument,
  ReviseOptions,
  FilterOrder,
  ResourceId,
  ResourceReference,
  ResourceMetadata,
} from '../database';

export type Index = string | null;
export type FirestoreDocument<T extends object = Document> = StoredDocument<T> & {
  _index?: Index;
};
export type FirestoreDocumentPath<T extends object = Document> = [
  FirestoreCollection<T>,
  ResourceId,
];
export type FirestoreBulkWriteAction<T extends object = Document> = [
  FirestoreDocumentPath<T>,
  BulkWriteOperation,
  Partial<StoredDocument<T>>?,
];

function collName(collection: Collection) {
  return `${collection.prefix || ''}${collection.name}`;
}

export class FirestoreCollection<T extends object = Document> implements Collection {
  name: CollectionName;

  prefix?: string;

  index: (keyof T)[];

  constructor({ name, prefix, index = [] }: Collection & { index?: (keyof T)[] }) {
    this.name = name;
    this.prefix = prefix;
    if (index.includes('updatedAt' as keyof T)) throw new TypeError('Invalid index on updatedAt');
    if (index.includes('_index' as keyof T)) throw new TypeError('Invalid index on _index');
    if (index.includes('id' as keyof T)) throw new TypeError('Invalid index on id');
    this.index = index;
  }

  calcIndex(document: T & ResourceMetadata): Index {
    if (this.index.length === 0) return `${nanoid()}${nanoid()}`;
    return this.index.map(key => document[key]).join('𝄄') || null;
  }
}

export class DatabaseNotInitializedError extends Error implements DatabaseError {
  message = 'Database not yet initialized!';
}

export class DocumentIndexConflictError extends Error implements DatabaseError {
  message = 'Document index conflicts';

  constructor(id: ResourceId) {
    super();
    this.message = `Document index conflict with document ${id}`;
  }
}
export class DocumentDoesNotExistError extends Error implements DatabaseError {
  message = 'Document does not exist';

  constructor(reference: ResourceReference) {
    super();
    this.message = `Document ${reference} does not exist`;
  }
}
export class ReviseConditionsUnmatchedError extends Error implements DatabaseError {
  message = 'Unmatched update condition(s)';
}
export class BulkWriteError extends Error implements DatabaseError {
  message = 'Bulk write error';
}

// HELPERS
function mapFilterOperator(operator: FilterQueryOperator): WhereFilterOp {
  return {
    '<': '<',
    '<=': '<=',
    '==': '==',
    '!=': '!=',
    '>=': '>=',
    '>': '>',
    in: 'in',
    '!in': 'not-in',
    includes: 'array-contains',
    includesSome: 'array-contains-any',
  }[operator] as WhereFilterOp;
}

function metadata<T extends object = Document>(document?: T | StoredDocument<T>): ResourceMetadata {
  const updatedAt = new Date(Date.now());
  if (document) {
    const { createdAt } = document as StoredDocument<T>;
    if (createdAt) return { createdAt, updatedAt };
  }
  return { createdAt: updatedAt, updatedAt };
}

function timestampsToDates<T extends object = Document>(snapshot: Record<string, any>) {
  for (const field in snapshot) { // eslint-disable-line no-restricted-syntax
    if (snapshot[field] instanceof Timestamp) {
      snapshot[field] = snapshot[field].toDate(); // eslint-disable-line no-param-reassign
    } else if (Array.isArray(snapshot[field])) {
      // eslint-disable-next-line no-param-reassign
      snapshot[field] = snapshot[field].map((sub: unknown) => {
        if (sub instanceof Timestamp) return sub.toDate();
        return timestampsToDates(sub as Record<string, any>);
      });
    } else if (snapshot[field] instanceof Object) {
      timestampsToDates(snapshot[field]);
    }
  }
  return snapshot as StoredDocument<T>;
}

// eslint-disable-next-line max-len
function output<T extends object = Document>(
  document: StoredDocument<T> & { _index?: Index },
): StoredDocument<T> {
  // eslint-disable-next-line no-underscore-dangle,no-param-reassign
  if (document._index) delete document._index; // remove _index
  return timestampsToDates<T>(document);
}

function documentFromSnapshot<T extends object = Document>(
  collection: Collection,
  document: DocumentSnapshot<T>,
): StoredDocument<T> {
  const data = document.data() as FirestoreDocument<T>;
  return {
    ...data,
    id: document.id,
    ref: document.ref.path.slice(collection.prefix?.length || 0),
  };
}

const defaultReviseOptions: ReviseOptions = { upsert: false, conditions: [] };

export class FirestoreDatabase implements Database {
  db: Firestore | null = null;

  private $dbOptions: Settings;

  constructor(options: Settings) {
    this.$dbOptions = options;
  }

  // LIFECYCLE METHODS
  initialize(): Promise<void> {
    this.db = new Firestore(this.$dbOptions);
    return Promise.resolve();
  }

  terminate(): Promise<void> {
    if (!this.db) return Promise.resolve();
    return this.db.terminate();
  }

  // CREATE OPERATIONS
  // eslint-disable-next-line max-len
  createOne<T extends object = Document>(
    collection: Collection,
    document: T,
    idOverwrite?: string,
  ): Promise<StoredDocument<T>> {
    const { db } = this;
    if (!db) throw new DatabaseNotInitializedError();
    const content = { ...document, ...metadata(document) };
    const index = new FirestoreCollection<T>(collection).calcIndex(content);
    return db.runTransaction(async t => {
      const { docs } = await t.get(db.collection(collName(collection)).where('_index', '==', index));
      if (docs[0]?.exists) throw new DocumentIndexConflictError(docs[0].id);
      const id = idOverwrite || nanoid();
      t.create(db.collection(collName(collection)).doc(id), { ...content, _index: index });
      return output({ ...content, id, ref: `${collection.name}/${id}` });
    });
  }

  async createMany<T extends object = Document>(
    creates: [Collection, T][],
  ): Promise<void> {
    if (!this.db) throw new DatabaseNotInitializedError();
    const actions = creates.map(([collection, document]) => {
      const documentPath = [collection, ''] as FirestoreDocumentPath;
      return [documentPath, 'create', document] as FirestoreBulkWriteAction;
    });
    return this.bulkWrite(actions);
  }

  // GET OPERATIONS
  async getOne<T extends object = Document>(
    collection: Collection,
    id: ResourceId,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>> {
    if (!this.db) throw new DatabaseNotInitializedError();
    let documentQuery = this.db.collection(collName(collection)).doc(id);
    if (projection) documentQuery = documentQuery.select(...projection);
    const document = await documentQuery.get();
    if (!document.exists) throw new DocumentDoesNotExistError(document.ref.path);
    return output(documentFromSnapshot(collection, document as DocumentSnapshot<T>));
  }

  async getMany<T extends object = Document>(
    documentPaths: DocumentPath[],
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>[]> {
    const { db } = this;
    if (!db) throw new DatabaseNotInitializedError();
    const references = documentPaths
      .map(([collection, id]) => db.collection(collName(collection)).doc(id));
    const documents = projection
      ? await db.select(...projection).getAll(...references)
      : await db.getAll(...references);
    return documents.map((document, n) => {
      if (!document.exists) throw new DocumentDoesNotExistError(document.ref.path);
      const [collection] = documentPaths[n];
      return output(documentFromSnapshot(collection, document as DocumentSnapshot<T>));
    });
  }

  async getAll<T extends object = Document>(
    collection: Collection,
    projection?: (keyof T)[]
  ): Promise<StoredDocument<T>[]> {
    if (!this.db) throw new DatabaseNotInitializedError();
     
    let docsQuery = this.db.collection(collName(collection))
    if (projection) docsQuery = docsQuery.select(...projection);
    const { docs } = await docsQuery.get();
  
    return docs.map(
      document => output(documentFromSnapshot(collection, document as DocumentSnapshot<T>)),
    );
  }

  // FIND OPERATION
  private async findWithLessThan30INs<T extends object = Document>(
    collection: Collection,
    queries: FilterQuery<T>[],
    orders: FilterOrder<T>[] = [],
    limit = 0,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>[]> {
    if (!this.db) throw new DatabaseNotInitializedError();
    // setup query
    let query = this.db.collection(collName(collection)) as Query<DocumentData>;
    // add further where clauses
    queries.forEach(q => {
      query = query.where(q[0] as any as FieldPath, mapFilterOperator(q[1]), q[2]);
    });
    // add order clauses
    orders.forEach(o => {
      query = query.orderBy(o[0] as any as FieldPath, o[1] || 'asc');
    });
    // add limit
    if (limit > 0) query = query.limit(limit);
    if (projection) query = query.select(...projection);
    // send query and reformat response
    const { docs } = await query.get();
    return docs.map(
      document => output(documentFromSnapshot(collection, document as DocumentSnapshot<T>)),
    );
  }

  // FIND OPERATION WITH >30 'IN' FILTER
  async find<T extends object = Document>(
    collection: Collection,
    queries: FilterQuery<T>[],
    orders: FilterOrder<T>[] = [],
    limit?: number,
    projection?: (keyof T)[],
  ) {
    const INfilterIndex = queries.findIndex(f => f[1] === 'in');
    const INfilter = queries.find(f => f[1] === 'in');
    // eslint-disable-next-line unicorn/explicit-length-check
    if (!INfilter || !INfilter[2].length || INfilter[2].length < 30) {
      return this.findWithLessThan30INs(collection, queries, orders, limit, projection);
    }
    const [field] = INfilter;

    // eslint-disable-next-line unicorn/no-array-reduce
    const chunks = (INfilter[2] as string[]).reduce<string[][]>((result, one, index) => {
      const number = Math.floor(index / 30);
      result[number] = [...(result[number] || []), one]; // eslint-disable-line no-param-reassign
      return result;
    }, []);

    const batches = chunks.map(filter => {
      const batchQuery = queries.map((q, n) => (
        n === INfilterIndex ? [field, 'in', filter] as [keyof ResourceMetadata, 'in', any] : q
      ));
      return this.findWithLessThan30INs(collection, batchQuery, orders, limit, projection);
    });

    const results = await Promise.all(batches);
    return results.flat(1).slice(0, limit);
  }

  // UPDATE HELPERS
  private async checkReviseConditions<T extends object = Document>(
    t: Transaction,
    collection: CollectionReference,
    id: DocumentId,
    conditions: FilterQuery<T>[],
  ) {
    const first = conditions[0];
    let reference = collection
      .where(first[0] as any as FieldPath, mapFilterOperator(first[1]), first[2]);
    // add further where clauses
    conditions.forEach(q => {
      reference = reference.where(q[0] as any as FieldPath, mapFilterOperator(q[1]), q[2]);
    });
    const documents = await t.get(reference);
    if (documents.size === 0 || !documents.docs.some(d => d.id === id)) {
      throw new ReviseConditionsUnmatchedError();
    }
  }

  // UPDATE OPERATIONS
  updateOne<T extends object = Document>(
    collection: Collection,
    id: ResourceId,
    document: T,
    options: ReviseOptions<T> = defaultReviseOptions,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>> {
    const { db } = this;
    if (!db) throw new DatabaseNotInitializedError();
    let coll = db.collection(collName(collection));
    if (projection) coll = coll.select(...projection);
    return db.runTransaction(async t => {
      const old = await t.get(coll.doc(id));
      if (!options.upsert && !old.exists) throw new DocumentDoesNotExistError(old.ref.path);
      if (options?.conditions?.length) { // abort if unmatched update conditions
        await this.checkReviseConditions(t, coll, id, options.conditions);
      }
      const content = { ...document, ...metadata(old.data()) };
      const index = new FirestoreCollection<T>(collection).calcIndex(content);
      const { docs } = await t.get(coll.where('_index', '==', index));
      if (docs[0]?.exists && docs[0].id !== id) throw new DocumentIndexConflictError(docs[0].id);
      t.set(coll.doc(id), { ...content, _index: index });
      return output({ ...content, id, ref: `${collection.name}/${id}` });
    });
  }

  async updateMany<T extends object = Document>(
    updates: [FirestoreDocumentPath, T][],
  ): Promise<void> {
    if (!this.db) throw new DatabaseNotInitializedError();
    const actions = updates.map(([documentPath, document]) => [documentPath, 'update', document] as FirestoreBulkWriteAction);
    return this.bulkWrite(actions);
  }

  // PATCH OPERATIONS
  patchOne<T extends object = Document>(
    collection: Collection,
    id: ResourceId,
    document: Partial<T>,
    options: ReviseOptions<T> = defaultReviseOptions,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>> {
    const { db } = this;
    if (!db) throw new DatabaseNotInitializedError();
    let coll = db.collection(collName(collection));
    if (projection) coll = coll.select(...projection);
    return db.runTransaction(async t => {
      const old = await t.get(coll.doc(id));
      if (!options.upsert && !old.exists) throw new DocumentDoesNotExistError(old.ref.path);
      if (options?.conditions?.length) { // abort if unmatched update conditions
        await this.checkReviseConditions(t, coll, id, options.conditions);
      }
      const oldContent = old.data() as FirestoreDocument<T>;
      // only send updates (patches to firestore)
      const content = { ...document, ...metadata(oldContent) };
      // but calculate index including old contents
      const index = new FirestoreCollection<T>(collection).calcIndex({ ...oldContent, ...content });
      const { docs } = await t.get(coll.where('_index', '==', index));
      if (docs[0]?.exists && docs[0].id !== id) throw new DocumentIndexConflictError(docs[0].id);
      if (options.upsert && !old.exists) t.set(coll.doc(id), { ...content, _index: index });
      else t.update(coll.doc(id), { ...content, _index: index });
      const outputable = assign({}, oldContent, content, { id, ref: `${collection.name}/${id}` });
      // if deep patching is applied, deep set the props on the output object
      const patches = {};
      Object.keys(content).filter(key => key.includes('.')).forEach(path => {
        set(patches, path, content[path as keyof T]);
        delete outputable[path as keyof T];
      });
      return output(merge(outputable, patches));
    });
  }

  async patchMany<T extends object = Document>(
    updates: [FirestoreDocumentPath, Partial<T>][],
  ): Promise<void> {
    if (!this.db) throw new DatabaseNotInitializedError();
    const actions = updates.map(([documentPath, value]) => [documentPath, 'patch', value] as FirestoreBulkWriteAction);
    return this.bulkWrite(actions);
  }

  // DELETE OPERATIONS
  deleteOne<T extends object = Document>(
    collection: Collection,
    id: ResourceId,
    options: ReviseOptions<T> = defaultReviseOptions,
  ): Promise<void> {
    const { db } = this;
    if (!db) throw new DatabaseNotInitializedError();
    const coll = db.collection(collName(collection));
    const reference = coll.doc(id);
    return db.runTransaction(async t => {
      const old = await t.get(reference);
      if (!old.exists) throw new DocumentDoesNotExistError(old.ref.path);
      if (options?.conditions?.length) { // abort if unmatched update conditions
        await this.checkReviseConditions(t, coll, id, options.conditions);
      }
      t.delete(reference);
    });
  }

  async deleteMany(documentPaths: FirestoreDocumentPath[]): Promise<void> {
    if (!this.db) throw new DatabaseNotInitializedError();
    const actions = documentPaths.map(documentPath => [documentPath, 'delete'] as FirestoreBulkWriteAction);
    return this.bulkWrite(actions);
  }

  // BULK OPERATION
  bulkWrite(actions: FirestoreBulkWriteAction[]): Promise<void> {
    const { db } = this;
    if (!db) throw new DatabaseNotInitializedError();
    return db.runTransaction(async t => {
      // check for conflicting indices
      const pre = await Promise.all(actions.map(async action => {
        const [[collection, id], operation, document] = action;

        // preconditions for create action
        if (operation === 'create') {
          const content = { ...document, ...metadata(document) };
          const index = collection.calcIndex(content);
          const { docs } = await t.get(db.collection(collName(collection)).where('_index', '==', index));
          if (docs[0]?.exists) throw new DocumentIndexConflictError(docs[0].id);
          return null;
        }

        // common preconditions for update/patch/delete actions
        const reference = db.collection(collName(collection)).doc(id);
        const old = await t.get(reference);
        if (!old.exists) throw new DocumentDoesNotExistError(old.ref.path);

        if (operation === 'update') { // precondition for update action
          const content = { ...document, ...metadata(old.data()) };
          const index = collection.calcIndex(content);
          const { docs } = await t.get(db.collection(collName(collection)).where('_index', '==', index));
          if (docs[0]?.exists && docs[0].id !== id) {
            throw new DocumentIndexConflictError(docs[0].id);
          }
        } else if (operation === 'patch') { // precondition for patch action
          const oldContent = old.data() as FirestoreDocument;
          // only send updates (patches to firestore)
          const content = { ...document, ...metadata(oldContent) };
          // but calculate index including old contents
          const index = collection.calcIndex({ ...oldContent, ...content });
          const { docs } = await t.get(db.collection(collName(collection)).where('_index', '==', index));
          if (docs[0]?.exists && docs[0].id !== id) {
            throw new DocumentIndexConflictError(docs[0].id);
          }
        }
        return old;
      }));

      // perform the update actions, if all preconditions succeed
      actions.forEach((action, index) => {
        const [[collection, id], operation, document] = action;
        const old = pre[index];

        // perform create action
        if (operation === 'create') {
          const content = { ...document, ...metadata(document) };
          const createIndex = collection.calcIndex(content);
          const randomId = nanoid();
          const reference = db.collection(collName(collection)).doc(randomId);
          t.create(reference, { ...content, _index: createIndex });
        }

        const reference = db.collection(collName(collection)).doc(id);
        if (!old) throw new BulkWriteError('precondition for update/patch/delete action is null');
        // perform update action
        switch (operation) {
          case 'update': {
            const content = { ...document, ...metadata(old.data()) };
            const updateIndex = collection.calcIndex(content);
            t.set(reference, { ...content, _index: updateIndex });
            break;
          }
          case 'patch': {
            const content = { ...document, ...metadata(old.data()) };
            const patchIndex = collection.calcIndex({ ...old.data(), ...content });
            t.update(reference, { ...content, _index: patchIndex });
            break;
          }
          case 'delete': {
            t.delete(reference);
            break;
          }
          default: {
            break;
          }
        }
      });
    });
  }
}

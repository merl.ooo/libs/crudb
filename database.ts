/* eslint-disable max-len */
export type ResourceReference = string;
export type ResourceId = string;

export type ResourceMetadata = {
  createdAt: Date;
  updatedAt: Date;
};

export type CollectionName = string;
export type Collection = {
  name: CollectionName;
  prefix?: string;
};

export type Document = Record<string, any>;
export type DocumentId = ResourceId;

export type StoredDocument<T extends object = Document> = T & { id: DocumentId, ref: ResourceReference } & ResourceMetadata;

export type DocumentPath = [Collection, DocumentId];

export type FilterQueryOperator =
  '<' | '<=' | '==' | '>' | '>=' | '!=' | 'in' | '!in' | 'includes' | 'includesSome';
export type FilterQuery<T extends object = Document> = [keyof (T & ResourceMetadata), FilterQueryOperator, any];
export type FilterOrder<T extends object = Document> = [keyof (T & ResourceMetadata), ('asc' | 'desc')?];

export type BulkWriteOperation = 'create' | 'update' | 'patch' | 'delete';
export type BulkWriteAction<T extends object = Document> = [
  DocumentPath,
  BulkWriteOperation,
  Partial<StoredDocument<T>>?,
];

export type ReviseOptions<T extends object = Document> = {
  upsert?: boolean,
  conditions?: FilterQuery<T>[],
};

export type Database = {
  // PLAIN DATABASE DRIVER
  db: unknown | null;

  // LIFECYCLE METHODS
  initialize(): Promise<void>;
  terminate(): Promise<void>;

  // CREATE OPERATIONS
  createOne<T extends object = Document>(collection: Collection, document: T, idOverwrite?: string): Promise<StoredDocument<T>>;
  createMany<T extends object = Document>(creates: [Collection, T][]): Promise<void>;

  // GET OPERATIONS
  getOne<T extends object = Document>(collection: Collection, documentId: DocumentId, projection?: (keyof T)[]): Promise<StoredDocument<T>>;
  getMany<T extends object = Document>(documentPaths: DocumentPath[], projection?: (keyof T)[]): Promise<StoredDocument<T>[]>;
  getAll<T extends object = Document>(collection: Collection, projection?: (keyof T)[]): Promise<StoredDocument<T>[]>;

  // FIND OPERATION
  find<T extends object = Document>(
    collection: Collection,
    queries: FilterQuery<T>[],
    orders?: FilterOrder<T>[],
    limit?: number,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>[]>;

  // UPDATE OPERATIONS
  updateOne<T extends object = Document>(
    collection: Collection,
    documentId: DocumentId,
    document: T,
    options?: ReviseOptions<T>,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>>;
  updateMany<T extends object = Document>(
    updates: [DocumentPath, T][],
    projection?: (keyof T)[],
  ): Promise<void | StoredDocument<T>[]>;

  // PATCH OPERATIONS
  patchOne<T extends object = Document>(
    collection: Collection,
    documentId: DocumentId,
    value: Partial<T>,
    options?: ReviseOptions<T>,
    projection?: (keyof T)[],
  ): Promise<StoredDocument<T>>;
  patchMany<T extends object = Document>(
    updates: [DocumentPath, Partial<T>][],
    projection?: (keyof T)[],
  ): Promise<void | StoredDocument<T>[]>;

  // DELETE OPERATIONS
  deleteOne<T extends object = Document>(collection: Collection, documentId: DocumentId, options?: ReviseOptions<T>): Promise<void>;
  deleteMany(documentPaths: DocumentPath[]): Promise<void>;

  // BULK OPERATION
  bulkWrite(actions: BulkWriteAction[]): Promise<void>;
};

export type DatabaseError = Error;
